<?php
function parse_signed_request($signed_request) {
    list($encoded_sig, $payload) = explode('.', $signed_request, 2);

    $secret = "edd945e47074f58c8fde789638e0081f"; // Use your app secret here

    // decode the data
    $sig = base64_url_decode($encoded_sig);
    $data = json_decode(base64_url_decode($payload), true);

    // confirm the signature
    $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
    if ($sig !== $expected_sig) {
        error_log('Bad Signed JSON signature!');
        file_put_contents('signed_request_error.txt', print_r('Bad Signed JSON signature!', true) );
        return null;
    }

    return $data;
}

function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}

function remove_instagram_user_data() {
    if(isset($_POST['signed_request'])) {
        file_put_contents('post.txt', print_r($_POST, true));
        header('Content-Type: application/json');
        file_put_contents('signed_request.txt', print_r($_POST['signed_request'], true));
        $signed_request = $_POST['signed_request'];
        $data = parse_signed_request($signed_request);
        file_put_contents('signed_request_data.txt', print_r($data, true));
        $user_id = $data['user_id'];
        //удаление пользователя с базы
        global $wpdb;
        $table_name = $wpdb->prefix . 'inst_feed_users';
        $wpdb->delete( $table_name, [ 'user_id' => $user_id ], [ '%s' ] );

//        $rID = rand();
        $status_url = 'https://www.otpaad.hard1.tech/deletion?id=' . $user_id; // URL to track the deletion
        $confirmation_code = $user_id; // unique code for the deletion request

        $responce_data = array(
            'url' => $status_url,
            'confirmation_code' => $confirmation_code
        );

         file_put_contents('signed_request_send_data.txt', print_r($responce_data, true));
        echo json_encode($responce_data);

        wp_die();
    }
}

add_action('init', 'remove_instagram_user_data' );


function create_table() {
    global $wpdb;

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';

    $table_name = $wpdb->get_blog_prefix() . 'inst_feed_users';
    $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";

    $sql = "CREATE TABLE {$table_name} (
	id  bigint(20) unsigned NOT NULL auto_increment,
	state_code varchar(255) NOT NULL default '',	
	redirect_url longtext NOT NULL default '',
	user_id varchar(255) NOT NULL default '',
	user_token longtext NOT NULL default '',
	user_login varchar(255) NOT NULL default '',
	profile_type varchar(50) NOT NULL default '',
	user_id_app bigint(20) unsigned NOT NULL, 
	PRIMARY KEY  (id),
	KEY state_code (state_code)
	)
	{$charset_collate};";

    dbDelta($sql);
}

add_action('init', 'create_table', 1);


function instagram_redirect() {
 if(isset($_GET['add_inst_user']) && $_GET['add_inst_user'] == 1 ) {
     if( isset($_GET['redirect']) ) {
         global $wpdb;
         $state_param = rand();
         file_put_contents('redirect.txt', print_r($state_param, true));

         $table = $wpdb->prefix . 'inst_feed_users';
         $count_records = $wpdb->get_var(
             $wpdb->prepare(
            "
			SELECT COUNT(*) 
			FROM {$table} 
			WHERE state_code = %s
		    ",
                 $state_param
             )
         );

         while ( $count_records > 0 ) {
             $state_param = rand();
             $count_records = $wpdb->get_var(
                 $wpdb->prepare(
                     "
			SELECT COUNT(*) 
			FROM {$table} 
			WHERE state_code = %s
		    ",
                     $state_param
                 )
             );
         }

         $wpdb->insert(
             $wpdb->prefix . 'inst_feed_users', // указываем таблицу
             array( // 'название_колонки' => 'значение'
                 'state_code' => $state_param,
                 'redirect_url' => $_GET['redirect']
             ),
             array(
                 '%s', // %s - значит строка
                 '%s'
             )
         );

//
//      file_put_contents('session.txt', print_r($_SESSION[$state_param], true));
        $instagram_app_id = '6279542665421361';
        $redirect_url = 'https://api.instagram.com/oauth/authorize?client_id=' . $instagram_app_id . '&redirect_uri=' . home_url('/') . '&scope=user_profile,user_media&response_type=code&state=' . $state_param;
        file_put_contents('instagram.txt', print_r($redirect_url, true));
//        session_write_close();
        wp_redirect($redirect_url);
        exit;
     }

 }
}

add_action('init', 'instagram_redirect');

add_action('init', 'redirect_to_site_with_plugin');

// изменить структуру таблицы, добавить поле с ID пользователя
// вставить код пользователя в таблицу по state code

function redirect_to_site_with_plugin() {
//    var_dump($_GET);
//    var_dump($_SESSION);
    if( isset($_GET['code']) && isset($_GET['state']) ) {
        file_put_contents('get.txt', print_r($_GET, true));
//        var_dump($_SESSION[$_GET['state']]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.instagram.com/oauth/access_token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=6279542665421361&client_secret=edd945e47074f58c8fde789638e0081f&grant_type=authorization_code&redirect_uri=https://otpaad.hard1.tech/&code={$_GET['code']}");
        $json_output = curl_exec($ch);
        $short_tok_output_array = json_decode($json_output, true);
        file_put_contents('inst_token-1.txt', print_r($short_tok_output_array, true) );
        curl_close($ch);

        $long_tok_output_array = [];

        if( isset( $short_tok_output_array['access_token']) ) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=edd945e47074f58c8fde789638e0081f&access_token={$short_tok_output_array['access_token']}");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $json_output = curl_exec($ch);
            $long_tok_output_array = json_decode($json_output, true);
            file_put_contents('inst_token_long.txt', print_r($long_tok_output_array, true) );
//            if(isset($long_tok_output_array['token_type']) && $long_tok_output_array['token_type'] == 'bearer' ) {
//                update_option( 'show_add_user_mes', 'yes', 'no' );
//                update_option( 'credentials_section_access_token', $output_array['access_token']);
//            }
        }
		
		if( isset( $short_tok_output_array['user_id']) && isset( $long_tok_output_array['access_token'])  ) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://graph.instagram.com/v12.0/" . $short_tok_output_array['user_id'] . "?fields=id,username,account_type&access_token=" . $long_tok_output_array['access_token']);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $user_profile_json = curl_exec($ch);
            $user_profile = json_decode($user_profile_json, true);
        }

        file_put_contents('user_profile.txt', print_r($user_profile, true) );

        // сохранить данные в базу (токен и id пользователя)
        global $wpdb;
        $table_name = $wpdb->prefix . 'inst_feed_users';

        $wpdb->update(
            $table_name, // указываем таблицу
            array('user_id' => $short_tok_output_array['user_id'],
                  'user_token' => $long_tok_output_array['access_token'],
                  'user_login' => $user_profile['username'],
                  'profile_type' =>  $user_profile['account_type'],                  
                  'user_id_app' => $user_profile['id'],
                 ), // добавляем USER_ID и USER TOKEN
            array( // где
                'state_code' => $_GET['state'],
            ),
            array( '%s', '%s', '%s', '%s', '%s' ),
            array( // формат для «где»
                '%s'
            )
        );

        $redir_data = $wpdb->get_row(
            "
	            SELECT *
	            FROM {$table_name}
	            WHERE state_code = {$_GET['state']}	            
	        "
        );

        // сделать переадресацию с уже готовым токеном
        if( isset($redir_data) ) {
            $url = $redir_data->redirect_url . '&new_code=1' . '&token_code=' . $redir_data->user_token . '&user_id=' . $redir_data->user_id . '&user_login=' . $redir_data->user_login . '&	profile_type=' . $redir_data->profile_type . '&user_id_app=' . $redir_data->user_id_app;
            file_put_contents('inst_code_and_url.txt', print_r($url, true));
            wp_redirect($url);
            exit;
        }
    }
}


/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *
 *   ВНИМАНИЕ!!!!!!!
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   ПРИ ОБНОВЛЕНИИ ТЕМЫ - ВЫ ПОТЕРЯЕТЕ ВСЕ ВАШИ ИЗМЕНЕНИЯ
 *   ИСПОЛЬЗУЙТЕ ДОЧЕРНЮЮ ТЕМУ ИЛИ НАСТРОЙКИ ТЕМЫ В АДМИНКЕ
 *
 *   ПОДБРОБНЕЕ:
 *   https://docs.wpshop.ru/yelly-child/
 *
 * *****************************************************************************
 *
 * @package Yelly
 */




/**
 * ВНИМАНИЕ!
 *
 * НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ,
 * ПРИ ОБНОВЛЕНИИ ВЫ ПОТЕРЯЕТЕ ВСЕ ВАШИ ИЗМЕНЕНИЯ
 *
 * ПОДБРОБНЕЕ:
 * https://docs.wpshop.ru/yelly-child/
 */






//wp_oembed_add_provider( 'http://instagr.am/*/*', 'http://api.instagram.com/oembed' );
//wp_oembed_add_provider( 'http://www.instagram.com/*/*', 'http://api.instagram.com/oembed' );

//wp_oembed_add_provider( 'https://instagr.am/*/*', 'http://api.instagram.com/oembed' );
//wp_oembed_add_provider( 'https://www.instagram.com/*/*', 'http://api.instagram.com/oembed' );








if ( ! function_exists( 'revelation_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function revelation_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Revelation, use a find and replace
	 * to change 'revelation' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'revelation', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
    //set_post_thumbnail_size( 330, 200, true );

    if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'thumb-small', 90, 70, true );
    }

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Верхнее', 'revelation' ),
		'secondary' => esc_html__( 'Нижнее', 'revelation' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		//'aside',
		//'image',
		'video',
		//'quote',
		//'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'revelation_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'revelation_setup' );


/**
 * Remove customizer header image
 */
add_action( 'init', 'remove_customizer_header_image' );
function remove_customizer_header_image() {
    remove_custom_image_header();
}


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function revelation_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'revelation_content_width', 640 );
}
add_action( 'after_setup_theme', 'revelation_content_width', 0 );




/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function revelation_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'revelation' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'revelation' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );
}
add_action( 'widgets_init', 'revelation_widgets_init' );





/**
 * Enqueue scripts and styles.
 */
function revelation_scripts() {
    $root_main_fonts = get_theme_mod( 'root_main_fonts', 'roboto' );
    $root_main_fonts_headers = get_theme_mod( 'root_main_fonts_headers', 'roboto' );

    $fonts = array(
        'open_sans'         => 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&amp;subset=cyrillic',
        'fira_sanscondensed'=> 'https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:400,400i,700&amp;subset=cyrillic',
        'merriweather'      => 'https://fonts.googleapis.com/css?family=Merriweather:400,400i,700&amp;subset=cyrillic',
        'roboto'            => 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700&amp;subset=cyrillic',
        'roboto_slab'       => 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=cyrillic',
        'roboto_condensed'  => 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700&amp;subset=cyrillic',
        'pt_sans'           => 'https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700&amp;subset=cyrillic',
        'ubuntu'            => 'https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700&amp;subset=cyrillic',
        'exo_2'             => 'https://fonts.googleapis.com/css?family=Exo+2:300,400,400i,700,900&amp;subset=cyrillic',
        'pt_serif'          => 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700&amp;subset=cyrillic',
        'tinos'          => 'https://fonts.googleapis.com/css?family=Tinos:400,400i,700&amp;subset=cyrillic',
    );

    if ( isset( $fonts[ $root_main_fonts ] ) ) {
        wp_enqueue_style('google-fonts', $fonts[ $root_main_fonts ], false);
    }

    if ( isset( $fonts[ $root_main_fonts_headers ] ) && $root_main_fonts != $root_main_fonts_headers ) {
        wp_enqueue_style('google-fonts-headers', $fonts[ $root_main_fonts_headers ], false);
    }


	wp_enqueue_style( 'revelation-style', get_template_directory_uri() . '/css/style.min.css', array(), null );
//	wp_enqueue_script( 'revelation-sticky', get_template_directory_uri() . '/js/sticky.js', array('jquery'), null, true );
//	wp_enqueue_script( 'revelation-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
function revelation_admin_scripts() {
	wp_enqueue_style( 'wp-color-picker' );
  //wp_enqueue_script( 'my-script-handle', plugins_url('my-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

	wp_enqueue_script('iris', admin_url('js/iris.min.js'),array('jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch'), false, 1);
    wp_enqueue_script('wp-color-picker', admin_url('js/color-picker.min.js'), array('iris'), false,1);
    $colorpicker_l10n = array('clear' => __('Clear'), 'defaultString' => __('Default'), 'pick' => __('Select Color'));
    wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n );

	wp_enqueue_style( 'revelation-admin-style', get_template_directory_uri() . '/css/style.admin.css', array(), null );
}
add_action( 'wp_enqueue_scripts', 'revelation_scripts' );
add_action( 'admin_enqueue_scripts', 'revelation_admin_scripts' );




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * WPShop.biz functions
 */
require get_template_directory() . '/inc/wpshopbiz.php';

/**
 * Clear WP
 */
require get_template_directory() . '/inc/clear-wp.php';

/**
 * Urlspan
 */
require get_template_directory() . '/inc/urlspan.php';

/**
 * Sitemap
 */
require get_template_directory() . '/inc/sitemap.php';

/**
 * Contact Form
 */
require get_template_directory() . '/inc/contact-form.php';

/**
 * Top commentators
 */
require get_template_directory() . '/inc/top-commentators.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Spoiler
 */
require get_template_directory() . '/inc/spoiler.php';

/**
 * TinyMCE
 */
if ( is_admin() ) {
    require get_template_directory() . '/inc/tinymce.php';
}

/**
 * Comments
 */
require get_template_directory() . '/inc/comments.php';


/**
 * Smiles
 */
require get_template_directory() . '/inc/smiles.php';


/**
 * Admin
 */
require get_template_directory() . '/inc/admin.php';


/**
 * Mobile Detect
 */
require get_template_directory() . '/inc/Mobile_Detect.php';



/**
 * YouTube Like
 */
require get_template_directory() . '/inc/youtube-like.php';


/**
 * Facebook UTM
 */
require get_template_directory() . '/inc/facebook-utm.php';


/**
 * Theme updater
 */
require get_template_directory() . '/inc/theme-update-checker.php';

$theme_name 		= 'yelly';

$revelation_options = get_option('revelation_options');
if ( isset($revelation_options['license']) && !empty($revelation_options['license']) ) {

	$update_checker = new ThemeUpdateChecker(
	    'yelly',
	    'https://api.wpgenerator.ru/wp-update-server/?action=get_metadata&slug='. $theme_name . '&license_key=' . $revelation_options['license']
	);

}




/********************************************************************
 * Editor styles
 *******************************************************************/
function revelation_add_editor_style() {
    add_editor_style( 'css/editor-styles.css' );
}
add_action( 'current_screen', 'revelation_add_editor_style' );



/********************************************************************
 * Excerpt
 *******************************************************************/
if ( ! function_exists( 'new_excerpt_length' ) ):
function new_excerpt_length($length) {
    return 28;
}
add_filter('excerpt_length', 'new_excerpt_length');
endif;

if ( ! function_exists( 'change_excerpt_more' ) ):
function change_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'change_excerpt_more');
endif;



/********************************************************************
 * Breadcrumbs
 *******************************************************************/
/**
 * Remove last item from breadcrumbs SEO by YOAST
 * http://www.wpdiv.com/remove-post-title-yoast-seo-plugin-breadcrumb/
 */
function adjust_single_breadcrumb( $link_output) {
    if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
        $link_output = '';
    }
    return $link_output;
}
add_filter('wpseo_breadcrumb_single_link', 'adjust_single_breadcrumb' );








/**
 * Remove page from single if it doesnt exists
 */
//add_action( 'template_redirect', 'wpshopbiz_remove_numbers_url', 0 );
function wpshopbiz_remove_numbers_url()
{
    if( is_singular() )
    {
        global $post, $page;
        $num_pages = substr_count( $post->post_content, '<!--nextpage-->' ) + 1;
        if ( $page > $num_pages || $page == 1){
            wp_safe_redirect(get_permalink( $post->ID ));
            exit;
        }
    }
}


/**
 * Remove h2 from pagination and navigation
 */
function change_navigation_markup_template( $template, $class ) {
    $template = '
	<nav class="navigation %1$s" role="navigation">
		<div class="screen-reader-text">%2$s</div>
		<div class="nav-links">%3$s</div>
	</nav>';
    return $template;
};

add_filter( 'navigation_markup_template', 'change_navigation_markup_template', 10, 2 );


/**
 * Remove word Category, Tag in archives
 */
add_filter( 'get_the_archive_title', 'get_the_archive_title_change');
function get_the_archive_title_change($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    }
    return $title;
}


/**
 * Remove pages from search
 *
 * @param $query
 * @return mixed
 */
function yelly_exclude_pages($query) {
    if ($query->is_search) {
        $query->set('post_type', 'post');
    }
    return $query;
}
add_filter('pre_get_posts','yelly_exclude_pages');


/**
 * Добавить разрыв строки
 */
if ( ! function_exists('mce_add_page_break') ) {
    add_filter('mce_buttons', 'mce_add_page_break');

    function mce_add_page_break($mce_buttons)
    {
        $pos_more = array_search('wp_more', $mce_buttons, true);

        if ($pos_more !== false) {
            $buttons = array_slice($mce_buttons, 0, $pos_more);
            $buttons[] = 'wp_page';
            $mce_buttons = array_merge($buttons, array_slice($mce_buttons, $pos_more));
        }

        return $mce_buttons;
    }
}




/**
 * Filter wp_link_pages to do both next and number
 */

add_filter('wp_link_pages_args', 'wp_link_pages_args_prevnext_add');
/**
 * Add prev and next links to a numbered link list
 */
function wp_link_pages_args_prevnext_add($args)
{
    global $page, $numpages, $more, $pagenow;
    if (!$args['next_or_number'] == 'next_and_number')
        return $args; # exit early
    $args['next_or_number'] = 'number'; # keep numbering for the main part
    if (!$more)
        return $args; # exit early
    /*if($page-1) # there is a previous page
        $args['before'] .= _wp_link_page($page-1)
            . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a>'
        ;*/
    if ($page<$numpages) # there is a next page
        $args['after'] = ' ' . _wp_link_page($page+1)
            . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
            . $args['after']
        ;
    return $args;
}





//wp_embed_register_handler( 'original_instagram', '/https?\:\/\/\S*?\instagram.com\/p\/(.+)/', 'WPTime_embed_instagram' );
function WPTime_embed_instagram( $matches, $attr, $url, $rawattr ) {
    $id = preg_replace( array("/[^&?]*?=[^&?]*/", "/[(?)]/", "/(\/p\/)/", "/(\/)/"), '', $matches[1] );
    $embed = sprintf('<div class="embed-insta"><img class="aligncenter wp-image" src="https://instagram.com/p/'.$id.'/media?size=l"><a href="https://instagram.com/p/'.$id.'/" target="_blank">Источник Instagram</a></div>');
    return apply_filters( 'original_instagram', $embed, $matches, $attr, $url, $rawattr );
}

add_shortcode( 'inst_feed', 'otpaad_show_user_feed_from_instagram' );

function otpaad_show_user_feed_from_instagram() {

//    $atts = shortcode_atts( [
//        'url' => '',
//    ], $atts );
    $url = "https://graph.instagram.com/me/media?fields=id,caption,media_type,permalink,timestamp,media_url&access_token=" . get_option('credentials_section_access_token') . "&limit=3";
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $json_output = curl_exec($ch);
    $output_array = json_decode($json_output, true);
    file_put_contents('feed.txt', print_r($output_array, true) );
    echo "<script>
            function moreImages() {
                let next = jQuery('#images').attr('data-next-images');
                jQuery.ajax({
                    url: 'https://graph.instagram.com/me/media?fields=id,caption,media_type,permalink,timestamp,media_url&access_token=" . get_option('credentials_section_access_token') . "&limit=3&next=' + next   
                }).done(function( images ) {
                    console.log(images);
                    jQuery('#images').attr('data-next-images', images['paging'].next);
                    let imagesBlock = jQuery('<div class=\'images-block\'></div>');
                    jQuery(images.data).each( function(indx, element) {
                        if(element.media_type == 'IMAGE') {
                            let a = jQuery('<a href=\'' + element.permalink + '\' target=\'_blanck\'></a>');
                            let img = jQuery('<img src=\'' + element.media_url + '\' style=\'margin-left: 10px; width: 30%; height: 250px; display: inline-block; object-fit: cover;\'>');
                            a.append(img);
                            imagesBlock.append(a);
                            imagesBlock.hide();
                            imagesBlock.fadeIn(900);
                        }
                    });
                    imagesBlock.css('margin-top', '20px');
                    jQuery('#images').append(imagesBlock);
                    if(!images.paging.next) {
                        jQuery('#moreImg').hide();
                    }
                    
                    
                });
            }
          </script>
    ";

    echo '<div id = "images" class="post-image">';
        echo '<div class="images-block">';
            for ($i=0; $i < count($output_array['data']); $i++ ) {
                echo '<a href="'. $output_array['data'][$i]['permalink'] .'" target="_blank"><img src="'. $output_array['data'][$i]['media_url'] .'" style="margin-left: 10px; width: 30%; height: 250px; display: inline-block; object-fit: cover;"></a>';
            }
        echo '</div>';

    echo '</div>';
    echo '<button id="moreImg" onclick="moreImages()" style="padding: 10px 20px; border: 3px solid rgb(24, 119, 242); display: block; width: 100px; border-radius: 10px; font-weight: 700; margin: 0px auto; font-size: 15px; cursor: pointer;">'. __('More', 'embed-video-from-inst') .'</button>';
    echo "<script>
            document.getElementById('images').setAttribute('data-next-images', '". $output_array['paging']['next']."');
          </script>
    ";
    echo '<style>
            
            @media screen and (max-width: 576px){
                #moreImg {
                    width: 150px !important;
                    font-size: 20px !important;
                }
                #images .images-block img {
                    width: 100% !important;
                    height: 500px !important;
                    margin-top: 20px;
                }
            }
          </style>  
    ';
}

add_shortcode( 'embed_inst_content', 'otpaad_add_embed_content_from_instagram' );

function otpaad_add_embed_content_from_instagram( $atts ){
    $atts = shortcode_atts( [
        'url' => '',
    ], $atts );
    $is_hide_caption = (bool) get_option( 'video_settings_section_hide_or_show_caption' ) ? 'true' : 'false';
    $video_width = get_option( 'video_settings_section_width_of_video' );
    $url = "https://graph.facebook.com/v12.0/instagram_oembed?url=" . $atts['url'] . '&hidecaption=true' . '&maxwidth=' . $video_width . '&access_token='. get_option( 'credentials_section_app_id' ). '|' . get_option( 'credentials_section_clients_marker' );
    file_put_contents('2.txt', print_r($url, true) . '/n/r', FILE_APPEND);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v12.0/instagram_oembed?url=" . $atts['url'] . '&hidecaption=' . $is_hide_caption . '&maxwidth=' . $video_width . '&access_token='. get_option( 'credentials_section_app_id' ). '|' . get_option( 'credentials_section_clients_marker' )  );
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $json_output = curl_exec($ch);
    $output_array = json_decode($json_output, true);

    file_put_contents('1.txt', print_r($output_array, true));
    return $output_array["html"];
}

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_style( 'main-css-file', get_stylesheet_uri() . '?' . rand(), [], null );
});

add_action( 'wp_head', function(){
    echo '<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4220247827992239" crossorigin="anonymous"></script>';
});

function hook_css(){
    echo '<style>.wp_head_example{ background-color : #f1f1f1; } </style>';
}