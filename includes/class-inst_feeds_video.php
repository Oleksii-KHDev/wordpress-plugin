<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 * @author     HarDevs <radchenko.hd@gmail.com>
 */
class Inst_feeds_video {
    
    protected $cache_table;
	
    /**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Inst_feeds_video_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	public function __construct() {
		if ( defined( 'INST_FEEDS_VIDEO_VERSION' ) ) {
			$this->version = INST_FEEDS_VIDEO_VERSION;
		} else {
			$this->version = '1.0.0';
		}

        if ( defined( 'TABLE_WITH_USER_FEEDS_CACHE' ) ) {
            $this->cache_table = TABLE_WITH_USER_FEEDS_CACHE;
        } else {
            global $wpdb;
            $this->cache_table = $wpdb->get_blog_prefix() . 'users_feeds_cache';            
        }
		
        $this->plugin_name = 'inst_feeds_video';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

        add_action('init', [$this, 'add_new_token']);

        add_action( 'update_option', [$this, 'ifv_add_task_to_update_feed'], 10, 3 );
        add_action( 'update_option', [$this, 'ifv_add_settings_to_update_feed_on_load_page'], 10, 3 );
        add_filter( 'cron_schedules', [$this, 'ifv_add_30_min_interval_to_cron'] );
        add_action('get_user_feed_from_inst', [$this, 'ifv_load_user_feed']);


        if( wp_doing_ajax() ) {
            add_action('wp_ajax_load_img_from_db', [$this, 'ajax_load_img_from_db']);
            add_action('wp_ajax_nopriv_load_img_from_db', [$this, 'ajax_load_img_from_db']);

        }

        add_shortcode( 'inst_feed', [$this, 'show_user_feed_from_instagram'] );
        add_shortcode( 'embed_inst_content', [$this, 'add_embed_content_from_instagram'] );

	}

    /**
     *
     * Convert structure of feed's array to display on page
     * @param array $images_db Images from DB
     * @return array Converted array
     */

	public function ifv_convert_structure_of_feeds($images_db) {
        $images_from_db = [];
	    foreach ($images_db as $image) {
            $images_from_db[] = ['id' => $image['image_id'],
                'caption' => $image['caption'],
                'media_type' => $image['media_type'],
                'permalink' => $image['permalink'],
                'timestamp' => $image['timestamp'],
                'media_url' => $image['media_url'],
                'thumbnail_url' => $image['thumbnail_url'],
                'username' => $image['username'],
            ];
        }

        return $images_from_db;
    }
	/**
     *
     * Set up option "last_cached_time" when the settings changed in the admin tab.
     * This option defines the feed refresh interval when the feed is refreshed on page load.
     *
     */

	public function ifv_add_settings_to_update_feed_on_load_page($option_name, $old_value, $new_value) {
        if ( 0 !== strcasecmp( $option_name, 'feed_settings_section_value_to_update_page' ) ) {
            return;
        }

        update_option('last_cached_time', time(), 'no' ); //устанавливаем время последнего обновления
        $this->deleteCachedImages(); // очищаем таблицу с сохраненным фидом
        $this->ifv_get_feeds_from_instagram(); //get feed and save to db
    }

	/**
     *
     * HOOKED TO get_user_feed_from_inst which fired by cron. Get feeds from Instagram and save it to DB.
     *
     */

	public function ifv_load_user_feed() {
        file_put_contents('cron.txt', 'it works!', FILE_APPEND);
        $this->deleteCachedImages();
        $this->ifv_get_feeds_from_instagram();

    }

	/**
     *
     * Adds a thirty minute interval to the cron scheduler
     * @param array $schedules Array with cron scheduler interval
     *
     */

	public function ifv_add_30_min_interval_to_cron($schedules) {
        $schedules['thirty_min'] = array(
            'interval' => 60 * 30,
            'display' => 'Раз в 30 минут'
        );
        return $schedules;
    }

	/**
     *
     * Add feeds updating task to wp cron when the option with background time interval changing
     *
     */

    public function ifv_add_task_to_update_feed ($option_name, $old_value, $new_value) {
        if ( 0 !== strcasecmp( $option_name, 'feed_settings_section_time_interval_to_update_back' ) ) {
            return;
        }


        wp_clear_scheduled_hook( 'get_user_feed_from_inst' );

        $cron_interval = wp_get_schedules();

        wp_schedule_event( time() + $cron_interval[$new_value]['interval'], $new_value, 'get_user_feed_from_inst');

        $this->deleteCachedImages();
        $this->ifv_get_feeds_from_instagram();

    }

	/**
     *
     * Gets User Feed from Instagram and save it to DB
     *
     */

	public function ifv_get_feeds_from_instagram() {
        $url = "https://graph.instagram.com/me/media?fields=id,caption,media_type,permalink,timestamp,media_url,username,thumbnail_url&access_token=" . get_option('credentials_section_access_token') . "&limit=";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json_output1 = curl_exec($ch);
        $output_array1 = json_decode($json_output1, true);
        file_put_contents('feed12.txt', print_r($output_array1, true));
        curl_close($ch);


        if ( isset($output_array1['data']) && count($output_array1['data']) >= 1 ) {
            $this->saveImagesToDB($output_array1['data']);
            while(isset($output_array1['paging']['next'])) {
                $url = "https://graph.instagram.com/me/media?fields=id,caption,media_type,permalink,timestamp,media_url,username,thumbnail_url&access_token=" . get_option('credentials_section_access_token') . "&limit=" . "&next=" . $output_array1['paging']['next'];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $json_output1 = curl_exec($ch);
                $output_array1 = json_decode($json_output1, true);
                file_put_contents('feed123.txt', print_r($output_array1, true));
                curl_close($ch);
                if ( isset($output_array1['data']) && count($output_array1['data']) >= 1 ) {
                    $this->saveImagesToDB($output_array1['data']);
                }
            }
        }
    }

    /**
     *
     * Directly gets the records of the user feeds from DB
     * @param int $offset Offset in the table from which to get records
     * @param int $number_of_records The number of records to be retrieved from the base.
     * @return array The array of record's data with user videos or images
     *
     */

	public function get_feeds_from_db ($offset, $number_of_records) {
        global $wpdb;
        $records_from_db = [];

        $images = $wpdb->get_results(
                "
                    SELECT * 
                    FROM {$this->cache_table}
                    LIMIT {$offset}, {$number_of_records} 
	            "
        );

        if($images && count($images) >= 1) {
            foreach ($images as $image) {
                $records_from_db[] = ['image_id' => $image->image_id,
                    'caption' => $image->caption,
                    'media_type' => $image->media_type,
                    'permalink' => $image->permalink,
                    'timestamp' => $image->timestamp,
                    'media_url' => $image->media_url,
                    'thumbnail_url' => $image->thumbnail_url,
                    'username' => $image->username,
                ];
            }
        }

        return $records_from_db;
    }

	/**
     *
     * Loads the user's feed from the database, as a response to a ajax request from the site's page
     * when the feed is displayed (hooked to
     *
     */

    public function ajax_load_img_from_db () {
        $record_from = $_POST['next'];
        $number_rec = $_POST['number_img'];
        $images_from_db = $this->get_feeds_from_db($record_from, $number_rec);

        echo json_encode($images_from_db);

        wp_die();
    }

	/**
     *
     * Caches information about feeds images and video into the database
     * @param array $images Array with params of each feed images.
     *
     */

	public function saveImagesToDB($images) {
        global $wpdb;
	    foreach ($images as $image) {
            $thumbnail_url = isset($image['thumbnail_url']) ? $image['thumbnail_url'] : '';
            file_put_contents('savetodb.txt', print_r($image, true), FILE_APPEND);

            $caption = isset($image['caption']) ?: '';
            $wpdb->query(
                $wpdb->prepare(
                    "
		                INSERT INTO {$this->cache_table}
		                ( image_id, media_type, permalink, media_url, timestamp, thumbnail_url, username, caption  )
		                VALUES ( %s, %s, %s, %s, %s, %s, %s, %s )
		            ",
                    array(
                        $image['id'],
                        $image['media_type'],
                        $image['permalink'],
                        $image['media_url'],
                        $image['timestamp'],
                        $thumbnail_url,
                        $image['username'],
                        $caption,
                    )
                )
            );
        }
	}

    /**
     *
     * Delete cached images from DB
     *
     */

    public function deleteCachedImages() {
	    global $wpdb;
        $wpdb->query("TRUNCATE TABLE {$this->cache_table}");
    }

	/**
     *
     * Show first page (part) of user feeds from Instagram or Database.
     * @param array $images Array with params of each feed images.
     * @param float $img_width Width each image on the page.
     * @param boolean $fromInst Get image from Instagram or Database
     * @param string $pagination link to the next portion of images from feed or database
     *
     */

    public function showFirstPartOfFeeds($images, $img_width, $fromInst, $pagination ) {
        echo '<div id = "images" class="post-image" style="text-align: center;">';


        $img_in_row = $fromInst ? count($images) : $pagination;

        for ($i = 0; $i < $img_in_row; $i++) {
            if($images[$i]['media_type'] == 'IMAGE') {
                echo '<a href="' . $images[$i]['permalink'] . '" target="_blank"><img src="' . $images[$i]['media_url'] . '" style="width: ' . $img_width . '%; min-height: ' . ($img_width * 8) . 'px; height: ' . ($img_width * 8) . 'px;"></a>';
            }
        }

        echo '</div>';

        echo '<button id="moreImg" onclick="newImg.getImage()">'. __('More', 'embed-video-from-inst') .'</button>';

        if($fromInst) {
            echo "<script>
                    document.getElementById('images').setAttribute('data-next-images', '". $pagination ."');
                  </script>";
        } else {
            echo "<script>
                    document.getElementById('images').setAttribute('data-next-images-db', '". $pagination ."');
                  </script>";
        }
    }

    public function show_user_feed_from_instagram () {

        global $wpdb;

        // вынести эту переменную как поле класса
        if(get_option('feed_settings_section_number_image_in_row')) {
            $img_in_row = (int)get_option('feed_settings_section_number_image_in_row');
        } else {
            $img_in_row = 3;
        }

        $img_width = (100 / $img_in_row - 2);

        // ПЕРЕППИСАТЬ КОД ДЛЯ ПОЛУЧЕНИЯ ВСЕХ ИЗОБРАЖЕНИЙ СРАЗУ
        // Получить изображения заполнить базу и выводить изображения с базы а не через постоянное обращение к Instagram

        if( !get_option('feed_settings_section_feed_update_interval') || strtolower(get_option('feed_settings_section_feed_update_interval')) == 'page'  ) {
            // получаем интервал обновления фида, когда фид обновляется при загрузке страницы
            $interval = 0;

            if(get_option('feed_settings_section_value_to_update_page')) {
                switch ( strtolower(get_option('feed_settings_section_time_interval_to_update_page')) ) {
                    case 'minutes':
                        $interval = 60 * get_option('feed_settings_section_value_to_update_page');
                        break;
                    case 'hours':
                        $interval = 60 * 60 * get_option('feed_settings_section_value_to_update_page');
                        break;
                    default:
                        $interval = 3600;
                }
            } else {
                $interval =  3600; // every hour
            }

            if( !get_option('last_cached_time') || ( get_option('last_cached_time') && ( time() - (int)get_option('last_cached_time') ) > $interval )) {

                file_put_contents('time.txt', time()  );
                $this->deleteCachedImages();
                $this->ifv_get_feeds_from_instagram();

                //create options with with last caching time
//                delete_option('last_cached_time');
//                add_option( 'last_cached_time', time(), '', 'no' );
                update_option('last_cached_time', time(), 'no');
                echo time();
                $images = $this->get_feeds_from_db(0, $img_in_row);
//                var_dump($images);
                if ($images && count($images) >= 1) {
                    foreach ($images as $image) {
                        $images_from_db[] = ['id' => $image['image_id'],
                            'caption' => $image['caption'],
                            'media_type' => $image['media_type'],
                            'permalink' => $image['permalink'],
                            'timestamp' => $image['timestamp'],
                            'media_url' => $image['media_url'],
                            'thumbnail_url' => $image['thumbnail_url'],
                            'username' => $image['username'],
                        ];
                    }
                    file_put_contents('feed12345.txt', print_r($images_from_db, true));
                    $this->showFirstPartOfFeeds($images_from_db, $img_width, false, $img_in_row);
                }
            } else {
                // Создать метод конвертирования структуры фида
                $images = $this->get_feeds_from_db (0, $img_in_row);

                if($images && count($images) >= 1) {
                    $images_from_db = $this->ifv_convert_structure_of_feeds($images);
                    file_put_contents('feed1234.txt', print_r($images_from_db, true));
                    $this->showFirstPartOfFeeds($images_from_db, $img_width, false, $img_in_row );
                }
            }
        } elseif( get_option('feed_settings_section_feed_update_interval') == 'background' ) {
            //написать функцию получения изображений с базы
            $images = $this->get_feeds_from_db (0, $img_in_row);

            if($images && count($images) >= 1) {
                $images_from_db = $this->ifv_convert_structure_of_feeds($images);
                file_put_contents('feed1234.txt', print_r($images_from_db, true));
                $this->showFirstPartOfFeeds($images_from_db, $img_width, false, $img_in_row );
            }
        }
    }

    public function add_embed_content_from_instagram ($atts) {
        $atts = shortcode_atts( [
            'url' => '',
        ], $atts );
        $is_hide_caption = (bool) get_option( 'video_settings_section_hide_or_show_caption' ) ? 'true' : 'false';

        if(get_option( 'video_settings_section_width_of_video' )) {
            $video_width = (int)get_option('video_settings_section_width_of_video');
        } else {
            $video_width = 326;
        }

        if($video_width > 658) {
            $video_width = 658;
        } elseif ($video_width < 326) {
            $video_width = 326;
        }

        $url = "https://graph.facebook.com/v12.0/instagram_oembed?url=" . $atts['url'] . '&hidecaption=true' . '&maxwidth=' . $video_width . '&access_token='. get_option( 'credentials_section_app_id' ). '|' . get_option( 'credentials_section_clients_marker' );
        file_put_contents('embed-video-url.txt', print_r($url, true));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v12.0/instagram_oembed?url=" . $atts['url'] . '&hidecaption=' . $is_hide_caption . '&maxwidth=' . $video_width . '&access_token='. get_option( 'credentials_section_app_id' ). '|' . get_option( 'credentials_section_clients_marker' )  );
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json_output = curl_exec($ch);
        file_put_contents('embed-video.txt', print_r($json_output, true));


        $output_array = json_decode($json_output, true);
        file_put_contents('embed-video-1.txt', print_r($output_array, true));


        if ( isset($output_array["html"]) ) {
            return '<div class="embed-video-block" style="width: ' . $video_width . 'px;">' . $output_array["html"] . '</div>';
        } else {
            return '';
        }
    }

    public function add_new_token() {
        // если в опциях есть профили пользователей, то выдаем запрос о добавлении или переключении аккаунтов, если нет - то просто добавляем

        if( isset($_GET['new_code']) && $_GET['new_code'] = 1 && isset($_GET['token_code']) )  {
            update_option( 'show_add_user_mes', 'yes', 'no' );
            //удалить устарела
            update_option( 'credentials_section_access_token', $_GET['token_code']);

            if ( !get_option('inst_users_profiles') || json_decode( get_option('inst_users_profiles') ) == [] ) {
                // показываем модальное окно
                update_option( 'show_add_or_change_feed_mes', 'no', 'no' );
                $inst_users_profiles = [];
            } else {
                update_option( 'show_add_or_change_feed_mes', 'yes', 'no' );
                $inst_users_profiles = json_decode( get_option('inst_users_profiles'), true );
            }

            // новый пользователь будет последним в массиве
            if ( isset($_GET['user_id']) ) {
                $inst_users_profiles[$_GET['user_id']] = [
                    'token_code' => $_GET['token_code'],
                    'user_login' => $_GET['user_login'],
                    'profile_type' => $_GET['profile_type'],
                    'user_id_app' => $_GET['user_id_app'],
                ];
                file_put_contents('inst_users_profiles.txt', print_r($inst_users_profiles, true) );
                update_option('inst_users_profiles', json_encode($inst_users_profiles), 'no' );
            }
        }
    }

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Inst_feeds_video_Loader. Orchestrates the hooks of the plugin.
	 * - Inst_feeds_video_i18n. Defines internationalization functionality.
	 * - Inst_feeds_video_Admin. Defines all hooks for the admin area.
	 * - Inst_feeds_video_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-inst_feeds_video-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-inst_feeds_video-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-inst_feeds_video-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-inst_feeds_video-public.php';

		$this->loader = new Inst_feeds_video_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Inst_feeds_video_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Inst_feeds_video_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		//поставить проверку, чтобы не загружать на фронте скрипты и стили для админки, проверить после
        // того, как перенесу стили и скрипты для админки в отдельный файл
	    $plugin_admin = new Inst_feeds_video_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		if( wp_doing_ajax() ) {
            $this->loader->add_action('wp_ajax_nopriv_on_delete_inst_profile', $plugin_admin, 'delete_user_inst_profile_from_site');
            $this->loader->add_action('wp_ajax_on_delete_inst_profile', $plugin_admin, 'delete_user_inst_profile_from_site');
            $this->loader->add_action('wp_ajax_nopriv_on_add_or_change_profile', $plugin_admin, 'add_to_feed_or_change_user_inst_profile');
            $this->loader->add_action('wp_ajax_on_add_or_change_profile', $plugin_admin, 'add_to_feed_or_change_user_inst_profile');
        }

        $this->loader->add_action( 'admin_print_footer_scripts', $plugin_admin, 'ifv_show_button_for_oembed_shortcode_in_redactor' );
		$this->loader->add_action( 'init', $plugin_admin, 'ifv_my_custom_format_script_register' );
		$this->loader->add_action( 'enqueue_block_editor_assets', $plugin_admin, 'ifv_my_custom_format_enqueue_assets_editor' );
		$this->loader->add_action( 'init', $plugin_admin, 'ifv_add_oembed_button_to_tinymce_editor' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Inst_feeds_video_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Inst_feeds_video_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
