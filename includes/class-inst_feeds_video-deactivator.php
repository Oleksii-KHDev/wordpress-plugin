<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 * @author     HarDevs <radchenko.hd@gmail.com>
 */
class Inst_feeds_video_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        delete_option('last_cached_time');
        wp_unschedule_hook('get_user_feed_from_inst');
//        wp_clear_scheduled_hook( 'get_user_feed_from_inst' );
	}

}
