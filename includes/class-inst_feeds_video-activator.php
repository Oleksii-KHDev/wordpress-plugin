<?php

/**
 * Fired during plugin activation
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/includes
 * @author     HarDevs <radchenko.hd@gmail.com>
 */
class Inst_feeds_video_Activator {
    protected $cache_table;

    public function __construct() {
        if ( defined( 'TABLE_WITH_USER_FEEDS_CACHE' ) ) {
            $this->cache_table = TABLE_WITH_USER_FEEDS_CACHE;
        } else {
            global $wpdb;
            $this->cache_table = $wpdb->get_blog_prefix() . 'users_feeds_cache';
        }
    }

    /**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        // Create table for instagram feeds cache
	    global $wpdb;
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        if ( defined( 'TABLE_WITH_USER_FEEDS_CACHE' ) ) {
            $cache_table = TABLE_WITH_USER_FEEDS_CACHE;
        } else {
            global $wpdb;
            $cache_table = $wpdb->get_blog_prefix() . 'users_feeds_cache';
        }

//      $table_name = $wpdb->get_blog_prefix() . 'users_feeds_cache';
        $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";

        $sql = "CREATE TABLE {$cache_table} (
	    id  bigint(20) unsigned NOT NULL auto_increment,
	    image_id varchar(255) NOT NULL default '',
	    media_type varchar(30) NOT NULL default '',
	    permalink longtext NOT NULL default '',
	    media_url longtext NOT NULL default '',
	    timestamp DATETIME NOT NULL default '0000-00-00 00:00:00',
	    thumbnail_url longtext NOT NULL default '' COMMENT 'Only for Video Posts',
	    username varchar(255) NOT NULL default '' COMMENT 'User who posted content',
	    caption varchar(255) NOT NULL default '' COMMENT 'Caption of Image', 
	    PRIMARY KEY  (id),
	    KEY image_id (image_id)
	    )
	    {$charset_collate};";

        dbDelta($sql);

        register_setting( 'embed_video_and_feeds', 'inst_users_profiles');

	}

}
