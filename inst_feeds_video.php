<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://hardevs.io/
 * @since             1.0.0
 * @package           Inst_feeds_video
 *
 * @wordpress-plugin
 * Plugin Name:       Instagram Feeds And Embed Video
 * Plugin URI:        https://hardevs.io/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            HarDevs
 * Author URI:        https://hardevs.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       inst_feeds_video
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
global $wpdb;

define( 'INST_FEEDS_VIDEO_VERSION', '1.0.0' );
define( 'TABLE_WITH_USER_FEEDS_CACHE', $wpdb->get_blog_prefix() . 'users_feeds_cache' );
define( 'ADMIN_PAGE_NAME', 'embed_video_and_feeds' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-inst_feeds_video-activator.php
 */
function activate_inst_feeds_video() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-inst_feeds_video-activator.php';
	Inst_feeds_video_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-inst_feeds_video-deactivator.php
 */
function deactivate_inst_feeds_video() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-inst_feeds_video-deactivator.php';
	Inst_feeds_video_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_inst_feeds_video' );
register_deactivation_hook( __FILE__, 'deactivate_inst_feeds_video' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-inst_feeds_video.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_inst_feeds_video() {

	$plugin = new Inst_feeds_video();
	$plugin->run();
}
run_inst_feeds_video();
