<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

delete_option('last_cached_time');
delete_option('credentials_section_app_id');
delete_option('credentials_section_clients_marker');
delete_option('credentials_section_access_token');
delete_option('video_settings_section_hide_or_show_caption');
delete_option('video_settings_section_width_of_video');
delete_option('feed_settings_section_hide_or_show_video');
delete_option('feed_settings_section_post_sort_order');
delete_option('feed_settings_section_number_image_in_row');
delete_option('feed_settings_section_is_show_dates');
delete_option('feed_settings_section_feed_update_interval');
delete_option('feed_settings_section_time_interval_to_update_back');
delete_option('feed_settings_section_time_interval_to_update_page');
delete_option('feed_settings_section_value_to_update_page');
delete_option('show_add_user_mes');
delete_option('inst_users_profiles');
delete_option('show_add_or_change_feed_mes');

wp_unschedule_hook('get_user_feed_from_inst');

global $wpdb;
$table_name = $wpdb->get_blog_prefix() . 'users_feeds_cache';
$wpdb->query("DROP TABLE IF EXISTS {$table_name}");



