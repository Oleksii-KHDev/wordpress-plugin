let newImg = (function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	function moreImages() {
		//Добавить функцию для вывода изображений на экран
		if( $('#images').is('[data-next-images-db]') ) {
			console.log('otput from db');
			let next = $('#images').attr('data-next-images-db');
			let data = {
				action: embed_video_data.action,
				next, // с какой записи начать
				number_img: embed_video_data.images_in_row, // сколько получить записей
			};
			$.post(embed_video_data.ajax_url, data, (images) => {
				console.dir(images);
				$('#images').attr('data-next-images-db', parseInt(next, 10) + parseInt(embed_video_data.images_in_row, 10) );

				$(images).each(function (indx, element) {
					if (element.media_type == 'IMAGE') {
						let a = $('<a href=\'' + element.permalink + '\' target=\'_blanck\'></a>');
						//                            let img = jQuery('<img src=\'' + element.media_url + '\' style=\'margin-left: 10px; width: " . (100 /(int)get_option('feed_settings_section_number_image_in_row') - 2) . "%; height: 250px; display: inline-block; object-fit: cover;\'>');
						let img = $('<img src=\'' + element.media_url + '\' style=\'width: ' + embed_video_data.width + '%; min-height: ' + embed_video_data.minHeight + 'px; height: ' + embed_video_data.height + 'px;\'>');
						a.append(img);
						// imagesBlock.append(a);
						$('#images').append(a);
						img.hide();
						img.fadeIn(900);
						img.css('margin-top', '20px');
					}
				});

				if ($(images).length < data.number_img) {
					$('#moreImg').hide();
				}

			}, "json");
		} else {
			if($('#images').is('[data-next-images]')) {
				let next = $('#images').attr('data-next-images');
				$.ajax({
					url: 'https://graph.instagram.com/me/media?fields=id,caption,media_type,permalink,timestamp,media_url&access_token=' + embed_video_data.token + '&limit=' + embed_video_data.images_in_row + '&next=' + next
				}).done(function (images) {
					console.log(images);
					$('#images').attr('data-next-images', images['paging'].next);
					// let imagesBlock = $('<div class=\'images-block\'></div>');
					$(images.data).each(function (indx, element) {
						if (element.media_type == 'IMAGE') {
							let a = $('<a href=\'' + element.permalink + '\' target=\'_blanck\'></a>');
							//                            let img = jQuery('<img src=\'' + element.media_url + '\' style=\'margin-left: 10px; width: " . (100 /(int)get_option('feed_settings_section_number_image_in_row') - 2) . "%; height: 250px; display: inline-block; object-fit: cover;\'>');
							let img = $('<img src=\'' + element.media_url + '\' style=\'width: ' + embed_video_data.width + '%; min-height: ' + embed_video_data.minHeight + 'px; height: ' + embed_video_data.height + 'px;\'>');
							a.append(img);
							// imagesBlock.append(a);
							$('#images').append(a);
							img.hide();
							img.fadeIn(900);
							img.css('margin-top', '20px');
						}
					});
					// imagesBlock.css('margin-top', '20px');
					// $('#images').append(imagesBlock);
					if (!images.paging.next) {
						$('#moreImg').hide();
					}


				});
			}
		}
	}
	return {
		getImage: moreImages
	}

})( jQuery );


