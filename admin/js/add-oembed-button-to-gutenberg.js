// (function (richText, element, editor) {
//     var el = element.createElement,
//         fragment = element.Fragment;
//
//     var registerFormatType = richText.registerFormatType,
//         unregisterFormatType = richText.unregisterFormatType,
//         toggleFormat = richText.toggleFormat;
//
//     var richTextToolbarButton = editor.RichTextToolbarButton,
//         richTextShortcut = editor.RichTextShortcut;
//
//     var type = "core/code";
//
//     unregisterFormatType(type);
//     registerFormatType(type, {
//         title:      "Code",
//         tagName:    "code",
//         className:  null,
//         edit: function edit(props) {
//             var isActive = props.isActive,
//                 value = props.value,
//                 onChange = props.onChange;
//
//             var onToggle = function() {
//                 return onChange(toggleFormat(value, { type: type }));
//             };
//
//             return el(
//                 fragment,
//                 null,
//                 el(richTextShortcut, {
//                     type: "access",
//                     character: "x",
//                     onUse: onToggle
//                 }),
//                 el(richTextToolbarButton, {
//                     icon: "editor-code",
//                     title: "Code",
//                     onClick: onToggle,
//                     isActive: isActive,
//                     shortcutType: "access",
//                     shortcutCharacter: "x"
//                 })
//             );
//         }
//     });
//
// }(
//     window.wp.richText,
//     window.wp.element,
//     window.wp.editor
// ));

( function ( wp ) {
    var MyCustomButton = function ( props ) {
        return wp.element.createElement( wp.blockEditor.RichTextToolbarButton, {
            icon: 'editor-video',
            title: 'Embed Inst Video',
            onClick: function () {
                // console.log(props.value);

                if(!props.isActive){
                    if(!props.value.text.includes('[embed_inst_content')) {
                        let shortcode = '[embed_inst_content url=""]';
                        let shortcode_len = shortcode.length;
                        let url_len = props.value.text.length;
                        props.value.end = shortcode_len + url_len;
                        let url = props.value.text;
                        props.value.text = '[embed_inst_content url="' + url + '"]';
                    }
                }
                // else {
                //     console.log(props.value.text);
                // }
                props.onChange(

                    wp.richText.toggleFormat( props.value, {
                        type: 'my-custom-format/sample-output',
                    } )
                );
            },
            isActive: props.isActive,
        } );
    };
    wp.richText.registerFormatType( 'my-custom-format/sample-output', {
        title: 'Embed Inst Video',
        tagName: 'emb',
        className: null,
        edit: MyCustomButton,
    } );
} )( window.wp );