 let func_for_adm = (function( $ ) {
	'use strict';

		function delete_user_profile(elem) {
			let profile_id = $(elem).parent().attr('data-profile-id');
			let data = {
				action: ifv_admin_data.action,
				profile_id
			};
			$.post(ifv_admin_data.ajax_url, data, () => {
				$(elem).parent().fadeOut('slow');
			});
		}

		function addNewProfileOrChange(act_type, profile_data) {

			console.dir(profile_data);
			let data = {
				action: ifv_admin_data.add_or_change_profile_act,
				type: act_type,
				profile: profile_data
			}
			console.dir(data);
			console.dir(ifv_admin_data.ajax_url);
			$.post(ifv_admin_data.ajax_url, data);
		}

		return {
			delete_user_profile,
			addNewProfileOrChange
		}


})( jQuery );
