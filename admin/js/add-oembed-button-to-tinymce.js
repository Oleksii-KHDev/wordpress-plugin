(function() {
    tinymce.create('tinymce.plugins.oembed_video_button', {
        init : function(ed, url) {
            ed.addButton('oembed_video', {
                title : 'Embed Video from Instagram',
                image : url + '/img/oembed_video.png',
                onclick : function() {
                    ed.selection.setContent('[embed_inst_content url="' + ed.selection.getContent() + '"]');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('oembed_video_button', tinymce.plugins.oembed_video_button);
})();