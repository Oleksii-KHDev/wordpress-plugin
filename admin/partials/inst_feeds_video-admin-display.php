<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/admin/partials
 */

function show_admin_page_options () {

    if( get_option('show_add_or_change_feed_mes') == 'yes' ) {
        $users_inst_prof = json_decode( get_option('inst_users_profiles'), true);

        $keys = array_keys($users_inst_prof);
        $last_profile_id = array_pop($keys );
        $last_profile = array_pop($users_inst_prof);
        ?>

        <script>
            let newUserProfile = {
                user_id: '<?= $last_profile_id ?>',
                token_code: '<?= $last_profile['token_code'] ?>',
                user_login: '<?= $last_profile['user_login'] ?>',
                profile_type: '<?= $last_profile['profile_type'] ?>',
                user_id_app: '<?= $last_profile['user_id_app'] ?>',
            }
        </script>

        <div class="modal" tabindex="-1" id="AddFeedModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">aleksei.zolotukhin</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="func_for_adm.addNewProfileOrChange('add', newUserProfile)">Добавить аккаунт</button>
                        <button type="button" class="btn btn-primary" onclick="func_for_adm.addNewProfileOrChange('change', newUserProfile)">Переключить аккаунт</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            let myModal = new bootstrap.Modal(document.getElementById('AddFeedModal'), {
                keyboard: true,
                focus: true
            });
            myModal.show();
        </script>
        <?php
    }
        ?>
    <h1><?= get_admin_page_title() ?></h1>

    <?php $url = 'https://otpaad.hard1.tech?redirect=' . home_url() . $_SERVER['REQUEST_URI'] . '&add_inst_user=1'; ?>
    <a id="add_inst" href="<?= $url ?>">Добавить аккаунт Instagram</a><br>
    <?php if(get_option( 'show_add_user_mes' ) == 'yes'): ?>
        <span style="color: red;">Аккаунт успешно добавлен:</span>
        <?php update_option( 'show_add_user_mes', 'no', 'no' ); ?>
    <?php endif; ?>
    <!-- Показывать пользовательские аккаунты -->
    <!-- Получить информацию по каждому добавленному аккаунту -->



    <form action="options.php" method="POST">
        <?php
        settings_fields( 'embed_video_and_feeds' );     // скрытые защитные поля
        do_settings_sections('embed_video_and_feeds'); // секции с настройками (опциями). У нас она всего одна 'section_id' credentials_section
        submit_button();
        ?>
    </form>
    <?php
}

function show_inst_user_accounts_section_inst_users_profiles() {

    $users_inst_prof = json_decode( get_option('inst_users_profiles'), true);
//


    if( get_option('inst_users_profiles') ) {

        //var_dump($users_inst_prof);
        foreach ($users_inst_prof as $userID => $data ) {
        ?>
            <div class="user-profile-set" data-profile-id="<?= $userID ?>">
                <div class="login-profile">
                    <div class="user-login">
                        <?= $data["user_login"]  ?>
                    </div>
                    <div class="profile-type">
                        <?= $data["profile_type"]  ?>
                    </div>
                </div>
                <div class="remove-from-feed" >
                    <i class="fas fa-user-slash"></i>Удалить с основного фида
                </div>
                <div class="add-to-feed">
                    <i class="fas fa-address-card"></i>Добавить к другому фиду
                </div>
                <div class="profile-settings">
                    <i class="fas fa-user-cog"></i>Настройки
                </div>
                <div class="remove-profile" onclick="func_for_adm.delete_user_profile(this)">
                    <i class="fas fa-user-times"></i>Удалить профиль
                </div>
            </div>
        <?php
        }
    }
}

function show_app_id_field_callback_function() {
    echo '<input
		name="credentials_section_app_id"
		type="text"
		value="' . get_option( 'credentials_section_app_id' ) . '"
		class="code2"/>';
}

function show_clients_marker_field_callback_function() {
    echo '<input
		name="credentials_section_clients_marker"
		type="text"
		value="' . get_option( 'credentials_section_clients_marker' ) . '"
		class="code2"/>';
}
function show_credentials_section_access_token_callback_function() {
    echo '<input
		name="credentials_section_access_token"
		type="text"
		value="' . get_option( 'credentials_section_access_token' ) . '"
		class="code2"/>';
}

function show_hide_or_show_caption_field_callback_function() {
    echo '<input
		name="video_settings_section_hide_or_show_caption"
		type="checkbox" 
		' . checked( 1, get_option( 'video_settings_section_hide_or_show_caption' ), false ) . '
		value="1"
		class="code"
	    />';
}
function show_video_settings_field_callback_function() {
    echo '<input
		name="video_settings_section_width_of_video"
		type="text"
		value="' . get_option( 'video_settings_section_width_of_video' ) . '"
		class="code2"/>';
}

function show_feed_settings_section_number_image_in_row() {
    echo '<input
		name="feed_settings_section_number_image_in_row"
		type="text"
		value="' . get_option( 'feed_settings_section_number_image_in_row' ) . '"
		class="code2"/>';
}

function show_feed_settings_section_post_sort_order_callback_function() {

    if (get_option( 'feed_settings_section_post_sort_order' ) == 'descending' ) {
        $descend = 'selected';
        $ascend = '';
    } elseif (get_option( 'feed_settings_section_post_sort_order' ) == 'ascending') {
        $descend = '';
        $ascend = 'selected';
    }


    echo '<select name="feed_settings_section_post_sort_order" class="code2">                                    
                <option value="descending" '. $descend .'>От новых к старым</option>   
                <option value="ascending" '. $ascend .'>От старых к новым</option>
              </select>   
        ';
}

function show_hide_or_show_video_in_user_feed_callback_function() {
    echo '<input
		name="feed_settings_section_hide_or_show_video"
		type="checkbox" 
		' . checked( 1, get_option( 'feed_settings_section_hide_or_show_video' ), false ) . '
		value="1"
		class="code"
	    />';
}

function show_feed_settings_section_is_show_dates() {
    echo '<input
		name="feed_settings_section_is_show_dates"
		type="checkbox" 
		' . checked( 1, get_option( 'feed_settings_section_is_show_dates' ), false ) . '
		value="1"
		class="code"
	    />';
}

function show_feed_settings_section_feed_update_interval() {
    if (get_option( 'feed_settings_section_feed_update_interval' ) == 'background') {
        $display_back_set = "block";
        $display_page_set = "none";
        $display_page_notif = "none";
        $display_back_notif = "block";
    } else {
        $display_back_set = "none";
        $display_page_set = "flex";
        $display_back_notif = "none";
        $display_page_notif = "block";
    }

    echo '
              <style>
                  .select-interval-back {
                    margin-top: 10px;
                  }
                  
                  .update-page-value {
                    width: 7%;
                    text-align: center; 
                  }
                    
                  .page-update-notif, .back-update-notif {
                    margin: 10px 0 10px;
                    
                    width: 70%;
                    color: green;
                    background: #e5eae1;
                    padding: 5px 10px;
                    border-radius: 5px;
                    border: 1px solid #ccd3c6;
                    font-size: 11px;
                    font-weight: bold; 
                  }
                  
                  .back-update-notif {
                    display: block;
                  }
                  
                  .backup-page-settings {
                     margin-top: 10px;
  
                  }
                  
                  .backup-page-settings span {
                    align-self: center;         
                  }
                  .back-updt-value {
                    display: block;
                  }
             </style>           
         
         <div id="all-back-updt-set">
             <input type="radio"'. checked('background', get_option( 'feed_settings_section_feed_update_interval' ), false ) . ' id="background"
                name="feed_settings_section_feed_update_interval" value="background">
             <label for="background">Фоновое обновление</label>
             <div class="back-updt-value" style="display: '. $display_back_set .';">
                <select name="feed_settings_section_time_interval_to_update_back" class="select-interval-back">
                    <option value="thirty_min"' . selected( get_option( 'feed_settings_section_time_interval_to_update_back' ), "thirty_min", false ) . '>Каждые 30 минут</option>
                    <option value="hourly"' . selected( get_option( 'feed_settings_section_time_interval_to_update_back' ), "hourly", false ) . '>Каждый час</option>
                    <option value="twicedaily"' . selected( get_option( 'feed_settings_section_time_interval_to_update_back' ), "twicedaily", false ) . '>Каждые 12 часов</option>  
                    <option value="daily"' . selected( get_option( 'feed_settings_section_time_interval_to_update_back' ), "daily", false ) . '>Каждые 24 часа</option>  
                </select>
                <span class="back-update-notif">
                    Сохранение настроек на этой странице очищает старый кэш и обновляет данные фида. Следующее обновление фида произойдет через указанный интервал времени. 
                </span>            
             </div>
             
         </div>
         <div id="all-page-updt-set">
            <input type="radio"'. checked('page', get_option( 'feed_settings_section_feed_update_interval' ), false ) . '  id="page"
            name="feed_settings_section_feed_update_interval" value="page">
            <label for="page">При загрузке страницы</label>
            <div class="backup-page-settings" style="display: '. $display_page_set .';">
                <span>Каждые:</span>
                <input class="update-page-value" name="feed_settings_section_value_to_update_page" type="number" value="'. get_option( 'feed_settings_section_value_to_update_page' ) .'" size="2" min="1">
                <select name="feed_settings_section_time_interval_to_update_page" class="select-interval-page">
                    <option value="minutes"' . selected( get_option( 'feed_settings_section_time_interval_to_update_page' ), "minutes", false ) . '>Минут</option>
                    <option value="hours"' . selected( get_option( 'feed_settings_section_time_interval_to_update_page' ), "hours", false ) . '>Часов</option>                  
                </select>
            </div>
            <span class="page-update-notif" style="display: ' . $display_page_notif . ';">
                    Сохранение настроек на этой странице очищает старый кэш и обновляет данные фида. Следующее обновление фида произойдет через указанный интервал времени. 
            </span>  
         </div>
         <script>
            jQuery(document).ready(function ($) {
////                let el = $("#all-back-updt-set");
////                console.log(el);
//                let backRadio = $("#all-back-updt-set input[type=radio]");
//                
//                if ( backRadio.is("[checked=checked]") ) {
//                    $(".backup-page-settings").hide();
//                    $(".page-update-notif").hide();                    
//                } else {
//                    $(".back-updt-value").hide();                    
//                }
//                
                $("#all-back-updt-set input[type=radio]").focus(function (){
                    $(".backup-page-settings, .page-update-notif").hide();
                    $(".back-updt-value").slideDown("slow");
                    $(".back-update-notif").slideDown("slow", () => {
                        $(".back-update-notif").css("display", "block");
                    });
//                    $(".back-updt-value").css("display", "block");
//                    $(".back-update-notif").css("display", "block");
                    
                    
//                    $("#all-back-updt-set input[type=radio]").attr("checkd", "checkd");
//                    $("#all-page-updt-set input[type=radio]").removeAttr("checkd");
                    
                });
//                
                $("#all-page-updt-set input[type=radio]").focus(function (){
                    $(".back-updt-value").slideUp("slow");
                    //                    $(".back-updt-value").hide();
                    $(".backup-page-settings, .page-update-notif").show();
                    $(".backup-page-settings").css("display", "flex");
                    $(".page-update-notif").css("display", "block");
                    $("#all-page-updt-set input[type=radio]").click();
                    
//                    $("#all-page-updt-set input[type=radio]").attr("checkd", "checkd");
//                    $("#all-back-updt-set input[type=radio]").removeAttr("checkd");
//                    $("#all-page-updt-set input[type=radio]").focus();
                });
            });
         </script>         
    ';
}
