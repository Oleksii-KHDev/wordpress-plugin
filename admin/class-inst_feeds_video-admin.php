<?php
require_once plugin_dir_path( __FILE__  ) . 'partials/inst_feeds_video-admin-display.php';
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://hardevs.io/
 * @since      1.0.0
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Inst_feeds_video
 * @subpackage Inst_feeds_video/admin
 * @author     HarDevs <radchenko.hd@gmail.com>
 */
class Inst_feeds_video_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;


        add_action('admin_menu', [$this, 'create_admin_option_page']);
        add_action( 'admin_init', [$this, 'create_fields_for_admin_page'] );
	}

    public function create_admin_option_page() {
        add_menu_page(
            'Настройки для добавления изображений и видео с Instagram', // тайтл страницы
            'Inst Video And Feeds', // текст ссылки в меню
            'manage_options', // права пользователя, необходимые для доступа к странице
            ADMIN_PAGE_NAME, // ярлык страницы
            'show_admin_page_options', // функция, которая выводит содержимое страницы
            'dashicons-images-alt2', // иконка, в данном случае из Dashicons
            "20.1"// позиция в меню
        );
    }



    public function create_fields_for_admin_page () {
        add_settings_section(
            'inst_user_accounts_section', // секция
            '',
            '',
            'embed_video_and_feeds' // страница
        );

	    add_settings_section(
            'credentials_section', // секция
            'Учетные данные для доступа в Instagram',
//             [$this, 'show_section_callback_function'],
            '',
            'embed_video_and_feeds' // страница
        );

        add_settings_section(
            'video_settings_section', // секция
            'Настройки подключаемого видео (встраивание)',
//             [$this, 'show_section_callback_function'],
            '',
            'embed_video_and_feeds' // страница
        );

        add_settings_section(
            'feed_settings_section', // секция
            'Нстройки отображения фида Instagram',
//             [$this, 'show_section_callback_function'],
            '',
            'embed_video_and_feeds' // страница
        );

        add_settings_field(
            'credentials_section_app_id',
            'ID приложения (для встраивания видео)',
            'show_app_id_field_callback_function', // можно указать ''
            'embed_video_and_feeds', // страница
            'credentials_section' // секция
        );
        add_settings_field(
            'credentials_section_clients_marker',
            'Маркер клиента (для встраивания видео)',
            'show_clients_marker_field_callback_function',
            'embed_video_and_feeds', // страница
            'credentials_section' // секция
        );

        add_settings_field(
            'credentials_section_access_token',
            'Токен для доступа к фиду пользователя (для отображения всех постов пользователя)',
            'show_credentials_section_access_token_callback_function',
            'embed_video_and_feeds', // страница
            'credentials_section' // секция
        );

        add_settings_field(
            'video_settings_section_hide_or_show_caption',
            'Скрыть подписи к видео?',
            'show_hide_or_show_caption_field_callback_function', // можно указать ''
            'embed_video_and_feeds', // страница
            'video_settings_section' // секция
        );

        add_settings_field(
            'video_settings_section_width_of_video',
            'Ширина видео-блока (мин. 320, макс. 658)',
            'show_video_settings_field_callback_function', // можно указать ''
            'embed_video_and_feeds', // страница
            'video_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_hide_or_show_video',
            'Показывать видео из фида пользователя?',
            'show_hide_or_show_video_in_user_feed_callback_function', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_post_sort_order',
            'Порядок сортировки постов пользователя:',
            'show_feed_settings_section_post_sort_order_callback_function', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_number_image_in_row',
            'Количество изображений в ряду:',
            'show_feed_settings_section_number_image_in_row', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_is_show_dates',
            'Показывать дату публикации?',
            'show_feed_settings_section_is_show_dates', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_feed_update_interval',
            'Интервал обновления фида',
            'show_feed_settings_section_feed_update_interval', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_time_interval_to_update_back',
            '',
            '', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_time_interval_to_update_page',
            '',
            '', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'feed_settings_section_value_to_update_page',
            '',
            '', // можно указать ''
            'embed_video_and_feeds', // страница
            'feed_settings_section' // секция
        );

        add_settings_field(
            'inst_users_profiles',
            'Аккаунты Instagram',
            'show_inst_user_accounts_section_inst_users_profiles', // можно указать ''
            'embed_video_and_feeds', // страница
            'inst_user_accounts_section' // секция
        );




        register_setting( 'embed_video_and_feeds', 'credentials_section_app_id' );
        register_setting( 'embed_video_and_feeds', 'credentials_section_clients_marker' );
        register_setting( 'embed_video_and_feeds', 'credentials_section_access_token' );
        register_setting( 'embed_video_and_feeds', 'video_settings_section_hide_or_show_caption' );
        register_setting( 'embed_video_and_feeds', 'video_settings_section_width_of_video' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_hide_or_show_video' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_post_sort_order' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_number_image_in_row' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_is_show_dates' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_feed_update_interval' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_time_interval_to_update_back' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_time_interval_to_update_page' );
        register_setting( 'embed_video_and_feeds', 'feed_settings_section_value_to_update_page' );
    }





	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inst_feeds_video_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inst_feeds_video_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/inst_feeds_video-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . '_awesome', plugin_dir_url( __FILE__ ) . 'css/fontawesome.min.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->plugin_name . '_awesome_solid', plugin_dir_url( __FILE__ ) . 'css/solid.min.css', array(), $this->version, 'all' );
        wp_enqueue_style( 'bootstrap_css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', array(), null, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Inst_feeds_video_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Inst_feeds_video_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/inst_feeds_video-admin.js', array( 'jquery' ), $this->version, false );

		wp_localize_script( $this->plugin_name, 'ifv_admin_data', array(
            'action' => 'on_delete_inst_profile',
            'add_or_change_profile_act' => 'on_add_or_change_profile',
            'ajax_url' => admin_url('admin-ajax.php'),
        ) );

//        wp_enqueue_script( 'oembed_gut_button', plugin_dir_url( __FILE__ ) . 'js/add-oembed-button-to-gutenberg.js', array( 'wp-element', 'wp-rich-text', 'wp-editor' ), $this->version, true );
        wp_enqueue_script( 'bootstrap-scripts', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', array(), null, false );

	}

    /**
     *
     * HOOKED TO on_delete_inst_profile action which fired when user delete inst profile on plugin's settings page
     *
     */

	public function delete_user_inst_profile_from_site() {
        if(isset($_POST['profile_id'])) {
            if( get_option('inst_users_profiles') ) {
                $profiles = json_decode(get_option('inst_users_profiles'), true);
                if ( isset($profiles[$_POST['profile_id']]) ) {
                    unset($profiles[$_POST['profile_id']]);
                    file_put_contents('delete-user-profile.txt', print_r($profiles, true) );
                    update_option('inst_users_profiles', json_encode($profiles), 'no' );
                }
            }
        }
    }

    /**
     *
     * HOOKED TO on_add_or_change_profile ajax action which fired when user
     * adds new inst profile to existing feed or changed profile in feed
     *
     */

    public function add_to_feed_or_change_user_inst_profile() {
        if($_POST['type'] == 'add') {
            $profiles = json_decode(get_option('inst_users_profiles'), true);
        } elseif ($_POST['type'] == 'change') {
            $profiles = [];
        }

        $profiles[$_POST['profile']['user_id']] = [
            'token_code' => $_POST['profile']['token_code'],
            'user_login' => $_POST['profile']['user_login'],
            'profile_type' => $_POST['profile']['profile_type'],
            'user_id_app' => $_POST['profile']['user_id_app'],
        ];

        update_option('inst_users_profiles', json_encode($profiles), 'no' );
        redirect(admin_url(ADMIN_PAGE_NAME));

        wp_die();
    }

    /**
     *
     * Hooked for action admin_print_footer_scripts
     * Show button in visual redactor for inserting oembed video shortcode
     *
     */

    public function ifv_show_button_for_oembed_shortcode_in_redactor() {
        if ( ! wp_script_is('quicktags') )
            return;

        ?>
        <script type="text/javascript">
            QTags.addButton( 'embed_inst_content', 'embed_video', '[embed_inst_content url="', '"]', 'o', 'Insert Oembed Video from Instagram', 1 );
        </script>

        <?php
    }

    public function ifv_my_custom_format_script_register() {
        file_put_contents('file.txt', plugin_dir_url( __FILE__ ) . 'js/add-oembed-button-to-gutenberg.js' );
        wp_register_script(
            'oembed_gut_button',
            plugin_dir_url( __FILE__ ) . 'js/add-oembed-button-to-gutenberg.js',
            array( 'wp-rich-text', 'wp-element', 'wp-editor' )
        );
    }

    public function ifv_my_custom_format_enqueue_assets_editor() {
        wp_enqueue_script( 'oembed_gut_button' );
    }

    /**
     *
     * Hooked to init action
     * Register button for inserting oembed video shortcode in tinymce editor
     *
     */

    public function ifv_add_oembed_button_to_tinymce_editor() {
        add_filter('mce_external_plugins', [$this, 'ifv_add_js_to_plugin_array']);
        add_filter('mce_buttons_3', [$this, 'ifv_register_new_tinymce_button']);
    }

    /**
     *
     * Hooked to mce_external_plugins filter
     *
     */

    public function ifv_add_js_to_plugin_array($plugin_array) {
            $plugin_array['oembed_video_button'] = plugin_dir_url( __FILE__ ) . 'js/add-oembed-button-to-tinymce.js';
        return $plugin_array;
    }

    /**
     *
     * Hooked to mce_buttons_3 filter
     *
     */

    public function ifv_register_new_tinymce_button($buttons) {
        array_push($buttons, "oembed_video");
        // место для продолжения задачь
        return $buttons;
    }
}
